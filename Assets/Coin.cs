using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public Transform Player;
    private bool Collected;
    private void OnEnable()
    {
        Player = FindObjectOfType<Player>().transform;
    }
    void MoveTowardPlayer()
    {
        transform.DOMove(Player.position, 0.3f).OnComplete(() =>
        {
            transform.DOScale(Vector3.zero, 0.1f).OnComplete(() =>
            {
                GameManager.instance.AddPoints(1); 
                Destroy(gameObject);
            });
        }).SetEase(Ease.InOutSine);
    }
    void Update()
    {
        if(Player ==null) return;
        if ((Vector3.Distance(transform.position, Player.position) < 1.75f) && !Collected)
        {
            MoveTowardPlayer();
            Collected = true;
        }

    }
}
