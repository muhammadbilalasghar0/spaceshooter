using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
   // public static HealthBar Instance;

    private void Awake()
    {
     //   Instance = this;
    }

    private Camera mainCam; 
    public Image damageFiller;

    private void Start()
    {
        mainCam = Camera.main;
    }

    public void TakeDamage(float Current, float Max)
    {
        StartCoroutine(Fill(Current, Max));
    }
    IEnumerator Fill(float Current, float Max)
    {
        float target = Current / Max;
        if (damageFiller.fillAmount < target)
        {
            while (damageFiller.fillAmount < target)
            {
                damageFiller.fillAmount += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
        }
        else
        {
            while (damageFiller.fillAmount > target)
            {
                damageFiller.fillAmount -= Time.deltaTime;
                yield return new WaitForEndOfFrame();
            } 
        }
    }
  
}
