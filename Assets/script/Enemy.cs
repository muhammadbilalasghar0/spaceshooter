using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 15.0f;
    public float _health;
    public HealthBar HB;
    public float MaxHealth;
    public float health
    {
        get => _health;
        set
        {
            _health = value;
            HB.TakeDamage(health,MaxHealth);

            
        }
    }
    [SerializeField] private float damageToPlayer = 20.0f;
    [SerializeField] private float damageRate = 0.2f;
    [SerializeField] private float damageTime;

    private void OnEnable()
    {
        MaxHealth = health;
    }

    public GameObject deathEffect;
    public GameObject Coin;

    // Update is called once per frame
    void Update()
    {
        if(GameManager.instance.gameState!=GameState.Playing) return;
        Movement();
    }

    private void Movement() {
        if (GameManager.instance.player) {
            transform.LookAt(GameManager.instance.player.transform.position);
            transform.position += transform.forward * moveSpeed * Time.deltaTime;
        }
    }

    public void TakeDamage (float damage){
        health -= damage;

        if(health <= 0){
            SpawnCoin();
            FindObjectOfType<Player>().EnemyKilled++;
            GameObject effect = Instantiate(deathEffect, transform.position, transform.rotation);
            Destroy(effect, 1.0f);
            Destroy(this.gameObject);
           // GameManager.instance.AddPoints(1);  // added via coin
        }
    }
void SpawnCoin()
{
     GameObject effect = Instantiate(Coin, transform.position, transform.rotation);
}
    void OnTriggerStay(Collider other){
        if(other.transform.tag == "Player" && Time.time > damageTime){
            other.transform.GetComponent<Player>().TakeDamage(damageToPlayer);
            damageTime = Time.time + damageRate;
        }
    }
}
