using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public enum GameState
{
    Playing,Shop
}
public class GameManager : MonoBehaviour
{
   

    public GameObject ShopPanel;
    public GameState _gameState;

    public GameState gameState
    {
        get
        {
            return _gameState;
        }
        set
        {
            _gameState = value;
            switch (value)
            {
                case GameState.Playing:
                    ShopPanel.SetActive(false);
                    break;
                case GameState.Shop:
                    ShopPanel.SetActive(true);

                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(value), value, null);
            }
        }
    }
    public static GameManager instance = null;
    public GameObject player;

    public Text scoreText;
    public int _score = 0;

    public int score
    {
        get => _score;
        set
        {
            _score = value;
            PlayerPrefs.SetInt("Coin",_score);
        }
    }

    void Start()
    {
        gameState = GameState.Playing;
        score = PlayerPrefs.GetInt("Coin", 0);
        SetScoreText();
    }

    void SetScoreText(){
        scoreText.text = "Score: " + score.ToString();
    }

    public void AddPoints(int scoreToAdd){
        score += scoreToAdd;
        Debug.Log(score);
        SetScoreText();
    }

    void Awake(){
        if (instance == null) {
            instance = this;
        }

        else if (instance != this) {
            Destroy(gameObject);
        }
    }
}
