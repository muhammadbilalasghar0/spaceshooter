using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 5.0f;
    [SerializeField] private float rotateSpeed = 360.0f;
    private Vector3 position;
    [SerializeField] private Vector3 _rotation;

    public int _EnemyKilled;
    public int EnemyKilled
    {
        get => _EnemyKilled;
        set
        {
            _EnemyKilled = value;
            if ((_EnemyKilled % 10) == 0)
            {
                GameManager.instance.gameState = GameState.Shop;
            }
        }
    }

    public float MaxHealth;
    public float _health;

    public float health
    {
        get => _health;
        set
        {
            _health = value;
           // HealthBar.Instance.TakeDamage(health,MaxHealth);

            
        }
    }

    public float bulletDamange;
    public GameObject deathEffect;

    private void Start()
    {
        health = 100;
        MaxHealth = health;
    }

    // Update is called once per frame
    void Update()
    {
        if(GameManager.instance.gameState!=GameState.Playing) return;

        MovePlayer();
    }
    //Player Input Controls
    private void MovePlayer()
    {
        //Forward & Backward Movement

        if (Input.GetKey(KeyCode.W)) transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
        if (Input.GetKey(KeyCode.S)) transform.Translate(Vector3.back * moveSpeed * Time.deltaTime);
        
        //Left and Right Rotation

        if (Input.GetKey(KeyCode.D)) _rotation = Vector3.up;
        else if (Input.GetKey(KeyCode.A)) _rotation = Vector3.down;
        else _rotation = Vector3.zero;
        transform.Rotate(_rotation * rotateSpeed * Time.deltaTime);
    }

    public void TakeDamage (float damage){
        health -= damage;
        if(health <= 0){
            SceneFader.instance.EndSceneWithName("assignment 2");

            GameObject effect = Instantiate(deathEffect, transform.position, transform.rotation);
            Destroy(effect, 1.0f);
            Destroy(this.gameObject);
        }
    }
}
