using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemyPrefab;
    [SerializeField] private float spawnRate = 2.0f;
    private float spawnTimer;
    public List<Transform> SpawnPoints;
    public float HealthOffset;

    // Update is called once per frame
    void Update()
    {
        if(GameManager.instance.gameState!=GameState.Playing) return;

        SpawnEnemy();
    }

    private void SpawnEnemy(){
        if (Time.time > spawnTimer){
           var obj= Instantiate(enemyPrefab, SpawnPoints[Random.Range(0,SpawnPoints.Count)].position, SpawnPoints[Random.Range(0,SpawnPoints.Count)].rotation);
           obj.GetComponent<Enemy>().health += HealthOffset;
           HealthOffset++;
            spawnTimer = Time.time + spawnRate;
        }
    }
}
