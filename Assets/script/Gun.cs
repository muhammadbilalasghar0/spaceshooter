using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    [SerializeField] private GameObject projectile;
    [SerializeField] private float fireRate = 0.1f;
    private float fireTime;

    private void Shoot()
    {
        if (Input.GetKey(KeyCode.Space) && Time.time > fireTime){
           var b= Instantiate(projectile, transform.position, transform.rotation);
           b.GetComponent<Projectile>().damage = FindObjectOfType<Player>().bulletDamange;
            fireTime = Time.time + fireRate;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Shoot();
    }
}
