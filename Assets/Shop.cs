using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
   public Image One;
   public Image Two;
   private void OnEnable()
   {
      if (GameManager.instance.score < 3)
      {
         One.GetComponent<Button>().interactable = false;
      }
      else
      {
         One.GetComponent<Button>().interactable = true;

      } if (GameManager.instance.score < 2)
      {
         Two.GetComponent<Button>().interactable = false;
      }
      else
      {
         Two.GetComponent<Button>().interactable = true;

      }

   }

   public void IncreaseHealth()
   {
      var player=FindObjectOfType<Player>();
      GameManager.instance.score -= 2;
      player.health = player.MaxHealth;
      GameManager.instance.gameState = GameState.Playing;

   }

   public void IncreaseProjectile()
   {
      GameManager.instance.score -= 3;

      var player=FindObjectOfType<Player>();
      player.bulletDamange += 10;
      GameManager.instance.gameState = GameState.Playing;

   }

   public void Back()
   {
      GameManager.instance.gameState = GameState.Playing;
   }
}
